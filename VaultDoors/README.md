# Vault Doors

Simple Java reverse engineering challenges.

Adapted from [picoCTF](https://github.com/picoCTF) 2019. All credits theirs

# Deploying the challenge

 1. Run `make` and let the makefile work its magic
 2. Distribute the jar found in `dist`
