import java.util.*;

public class Main {
	public static void main(String args[]) {
		int num = 0;
		do {
			System.out.println("Which vault door do you want to try to open? [1-7]");
			Scanner scanner = new Scanner(System.in);
			String input = scanner.next();
			try {
            	num = Integer.parseInt(input);
        	}
        	catch (NumberFormatException ex){
				System.out.println("Invalid input. Please enter an integer between 1 and 7.");
        	}
		} while (num < 1 || num > 7);

		switch (num) {
			case 1:
				VaultDoor1.main(args);
				break;
			case 2:
				VaultDoor2.main(args);
				break;
			case 3:
				VaultDoor3.main(args);
				break;
			case 4:
				VaultDoor4.main(args);
				break;
			case 5:
				VaultDoor5.main(args);
				break;
			case 6:
				VaultDoor6.main(args);
				break;
			case 7:
				VaultDoor7.main(args);
				break;
		}
	}
}
