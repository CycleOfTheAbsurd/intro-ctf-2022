/*
 * Decompiled with CFR 0.151.
 */
public class Main {
    public static void main(String[] stringArray) {
        int n = 34;
        boolean bl = false;
        for (int i = 2; i <= n / 2; ++i) {
            if (!Main.checkPrime(i) || !Main.checkPrime(n - i)) continue;
            System.out.printf("%d = %d + %d\n", n, i, n - i);
            bl = true;
        }
        if (!bl) {
            System.out.println(n + " cannot be expressed as the sum of two prime numbers.");
        }
    }

    static boolean checkPrime(int n) {
        boolean bl = true;
        for (int i = 2; i <= n / 2; ++i) {
            if (n % i != 0) continue;
            bl = false;
            break;
        }
        return bl;
    }
}
