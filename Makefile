all: slides.pdf challenges

slides.pdf: slides.md
	pandoc $< -o $@ -t beamer

challenges:
	make -C VaultDoors

clean:
	rm -rf *.pdf
	make -C VaultDoors clean
