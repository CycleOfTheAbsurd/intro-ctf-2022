---
title: "Introduction aux CTFs"
author: "Barberousse"
institute: "Présenté à l'UQAM - H2022"
theme: "Malmoe"
header-includes:
  - \hypersetup{colorlinks=true}
---

# Pourquoi moi?

Barberousse (aka Alex)

 - Fini mon Bacc à l'UQAM en H2021
 - Faire de la sécu
	- Participé à Northsec, Hackfest, CSAW. CSGAMES, etc. avec l'AGEEI
	- Malware Researcher chez ESET depuis Mai 2021
 - Enseigner/introduire à la sécu
	- Démonstrateur INF600C 2018-2021
	- Ex Bureau des Compétitions
	- Organisateur UnitedCTF 2020-2021


# Plan pour aujourd'hui

 1. C'est quoi un CTF
 2. Catégories communes
 3. Survol de la rétro-ingénierie de programmes Java
 4. Exercices appliqués
 5. Varia et list de ressources pour aller plus loin

**N'hésitez pas à poser des questions en tout temps**


# On parle de quoi?

**C**apture **T**he **F**lag

Compétition de sécurité avec des failles intentionnelles à exploiter et des défis à résoudre dans un environnement contrôlé.

Le but est d'obtenir une information secrète (_flag_) pour prouver une exploitation réussie.

Très grande variété de formules et modalités (_Jeopardy_/À thème/_Attack-defense_, Permanent/Temporaire, En équipe/Solo, On-site/En ligne...)


# Ça somme comme un TP. Pourquoi je ferais ça?

 - Très formateur
 - Jobs en sécurité et networking
 - Actually fun
 - Enfin être un.e hacker comme on a toustes voulu.e.s l'être depuis qu'on a vu \[The Matrix, Mr. Robot, Wargames, Hackers, Edward Snowden...\]


# Qu'est-ce que ça implique concrètement?

 - Exploitation Web
 - Cryptographie
 - Rétro-ingénierie (Reverse Engineering)
 - Exploitation binaire (Pwn)
 - Forensique


# Exploitation Web

![](img/hacker.jpg){ height=200px }

Exploiter des failles dans un site web.

Souvent sans accès au code source (PHP, Ruby, Python, Javascript, Java, SQL, Perl et autres plus ésotériques)


# Exploitation Web

### Connaissances Utiles:

 - Protocole HTTP
 - Fonctionnement des sites web et serveurs web
 - Base de données

### Outils:

 - Proxy intercepteur ([Burp](https://portswigger.net/burp), [ZAP](https://github.com/zaproxy/zaproxy))
 - `curl`/un browser


# Cryptographie

![](img/crypto.jpg){ height=200px }

Déchiffrer un message chiffré.

Algorithmes existants ou custom. Généralement basé sur une mauvaise utilisation ou une faiblesse dans l'algorithme.


# Cryptographie

### Connaissances Utiles:

 - Cryptologie
 - Bibliothèques de crypto
 - Algorithmie/maths

### Outils:

 - Langages de programmation
 - Maths
 - Un cryptologue si possible


# Exploitation binaire (aka _Pwn_)

![](img/xkcd_segfault.png)

Exploiter une faille programme compilé.

Généralement, on a le programme compilé, et parfois le code source.


# Exploitation binaire (aka _Pwn_)

### Connaissances Utiles:

 - C et autres langages de bas niveau
 - Assembleur
 - Systèmes d'exploitation (appels systèmes, chargement des programmes)
 - Gestion de la mémoire (stack, heap, allocation/désallocation, pointeurs)
 - Conventions d'appel (passage d'argument, registres utilisés, valeurs de retour)

### Outils:

 - Debugger (`gdb`/[gef](https://github.com/hugsy/gef), [x64dbg](https://github.com/x64dbg/x64dbg))
 - Outils de traçage (`strace`, `ltrace`, [Procmon](https://docs.microsoft.com/en-us/sysinternals/downloads/procmon))
 - Machine virtuelle ([VirtualBox](https://www.virtualbox.org/), [QEMU](https://www.qemu.org/))


# Forensique

![](img/forensics.jpg){ height=200px }

CSI d'ordinateur. Comprendre ce qui s'est passé à partir des traces laissées.


# Forensique

 - Logiciels de capture et d'analyse de paquets ([Wireshark](https://www.wireshark.org/), [tcpdump](https://www.tcpdump.org/))
 - Récupération d'artéfacts sur le disque/mémoire ([Autopsy](https://sleuthkit.org/autopsy/), [Volatility](https://www.volatilityfoundation.org/), [Scalpel](https://github.com/sleuthkit/scalpel))
 - Capture d'évènements/comportements (Logs en tout genre)


# Rétro-ingénierie (Reverse Engineering)

![](img/reverse.jpg){ height=200px }

**Ingénierie** = Idée/Objectif --> Produit fini

**Rétro-Ingénierie** = Idée/Objectif <-- Produit fini

Comprendre le fonctionnement d'un programme donné. Classiquement un binaire compilé C, mais s'applique à tout programme/langage


# Rétro-ingénierie (Reverse Engineering)

### Connaissances Utiles:

 - Concepts de programmation (boucles, récursion, conditionnels, etc.)
 - Assembleur
 - Systèmes d'exploitation (appels systèmes, chargement des programmes)
 - Conventions d'appel (passage d'argument, registres utilisés, valeurs de retour)

### Outils:

 - Analyse Statique
	- Décompilateur/désassembleur ([Ghidra](https://github.com/NationalSecurityAgency/ghidra), [Radare2](https://github.com/radareorg/radare2), [dnSpy](https://github.com/dnSpy/dnSpy), [jadx](https://github.com/skylot/jadx), `objdump`, [IDA](https://hex-rays.com/IDA-pro/))
	- Extraction d'artéfacts (`strings`, [binwalk](https://github.com/ReFirmLabs/binwalk))
	- Émulation ([Unicorn Engine](https://github.com/unicorn-engine/unicorn), [Miasm](https://github.com/cea-sec/miasm), [Angr](https://github.com/angr/angr))
 - Analyse Dynamique
	- Debugger (`gdb`/[gef](https://github.com/hugsy/gef), [x64dbg](https://github.com/x64dbg/x64dbg))
	- Monitoring (`ptrace`, [Procmon](https://docs.microsoft.com/en-us/sysinternals/downloads/procmon), [Wireshark](https://www.wireshark.org/), [inetsim](https://www.inetsim.org/), [mitmproxy](https://mitmproxy.org/))


# Assez parlé, on fait quoi aujourd'hui?

![](img/java.png){ height=200px }

### Rétro-ingénierie Java

Compilé en bytecode pour la JVM -> Conserve plus de métadonnées et d'informations. La décompilation permet donc de récupérer du code près de l'original.


# Démonstration

| Original | Décompilé par `cfr` |
|----------|---------------------|
|[Main.java](./Main.java) | [Main.cfr.java](Main.cfr.java)


# Exercices

![](img/nsec_2018.jpg){ height=100% }


# Ressources

 - [Le repo de ressources du Bureau des compétitions](https://gitlab.com/ageei/compes/bureau-des-competitions/ressources-intro-ctf)
 - [CTF101](https://ctf101.org/)
 - [Awesome CTF](https://github.com/apsdehal/awesome-ctf)
 - [Hacking, The Art of Exploitation](https://nostarch.com/hacking2.htm)
 - [Practical Malware Analysis](https://nostarch.com/malware)
	 - Et plusieurs autres livres de No Starch Press
